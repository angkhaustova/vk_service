import React from 'react';
import PropTypes from 'prop-types';

import {
  View,
  Panel,
  Alert,
  ActionSheet,
  ActionSheetItem,
  CellButton,
  Group,
  ListItem,
  PanelHeader,
  HeaderButton,
  platform,
  IOS,
  Cell,
  List,
  Button,
  Div,
  Input,
  FormLayout,
  FormLayoutGroup
} from '@vkontakte/vkui';

import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import Icon24Place from '@vkontakte/icons/dist/24/place';
import Icon24Info from '@vkontakte/icons/dist/24/info';

const osname = platform();

class Task extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.cardId,
      code: this.props.cardCode,
      date: this.props.cardId,
      popout: null
    };
  }

  openSheet() {
    this.props.setPopout(
      <Alert
        actions={[
          {
            title: 'Close',
            autoclose: true,
            style: 'destructive'
          },
          {
            title: 'Cancel',
            autoclose: true,
            style: 'cancel'
          }
        ]}
        onClose={() => this.props.setPopout(null)}
      >
        <h2>Hi!</h2>
        <p>I am alert</p>
      </Alert>
    );
  }

  render() {
    const props = this.props;
    const data = props.cardId;
    return (
      <Panel id={props.id}>
        <PanelHeader
          left={
            <HeaderButton onClick={e => props.go(e, data.code)} data-to="page">
              {osname === IOS ? <Icon28ChevronBack /> : <Icon24Back />}
            </HeaderButton>
          }
        >
          {data.name}
        </PanelHeader>
        <Group>
          <Div>
            <List>
              <Cell before={<Icon24Info />}>{data.target}</Cell>
              <Cell before={<Icon24Place />}>{data.adress}</Cell>
              {data.task}
            </List>
          </Div>
          <FormLayout>
            <FormLayoutGroup top="Введите ответ">
              <Input type="text" defaultValue="" alignment="center" />
            </FormLayoutGroup>
          </FormLayout>

          <ListItem>
            <Button size="l" stretched onClick={props.go} data-to="page">
              Проверить
            </Button>
          </ListItem>

          <CellButton onClick={this.openSheet.bind(this)}>Подсказка</CellButton>
        </Group>
      </Panel>
    );
  }
}

Task.propTypes = {
  id: PropTypes.string.isRequired,
  go: PropTypes.func.isRequired
};

export default Task;
